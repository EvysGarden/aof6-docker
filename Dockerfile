# download and prepare
FROM alpine:latest as source
ENV MODPACK_URL="https://mediafilez.forgecdn.net/files/4400/194/All-of-Fabric-6-Server-1.4.3.zip"
ENV SERVER_STARTER_URL="https://github.com/TeamAOF/ServerStarter/releases/download/v2.4.0/serverstarter-2.4.0.jar"
WORKDIR /source
RUN apk upgrade --update-cache --available && \
    apk add wget openssl
RUN wget ${MODPACK_URL} -O server.zip
RUN wget ${SERVER_STARTER_URL} -O serverstarter.jar
RUN unzip server.zip
RUN rm server.zip

# run
EXPOSE 25565/tcp
FROM alpine:latest as run
WORKDIR /instance
RUN apk upgrade --update-cache --available && \
    apk add --no-cache openjdk17 openjdk17
COPY --from=source /source/server-setup-config.yaml /instance/server-setup-config.yaml
COPY --from=source /source/serverstarter.jar /source/serverstarter.jar
COPY start.sh /instance/start.sh
RUN chmod +x /instance/start.sh
ENTRYPOINT exec /instance/start.sh
VOLUME /instance
